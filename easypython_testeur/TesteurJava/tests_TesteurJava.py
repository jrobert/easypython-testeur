__author__ = 'julien'
from easypython_testeur.TesteurJava import *

if __name__=="__main__":
    codeEns = b"""
    import junit.framework.*;
    public class MaClasseTest extends TestCase{
      public void testCalculer() throws Exception {
              assertEquals(2,MaClasse.calculer(1,1.));
                }
      public void testCalculer2() throws Exception {
              assertEquals("Sur l'entree (1,1), votre fonction n'a pas fait ce qui etait attendu",3,MaClasse.calculer(1,1));
                }
      public void testCalculer3() throws Exception {

              assertEquals("Sur l'entree (1,1), votre fonction n'a pas fait ce qui etait attendu",4,MaClasse.calculer(1,1));
                }
      public void testCalculer4() throws Exception {
              assertEquals("Sur l'entree (1,1), votre fonction n'a pas fait ce qui etait attendu",2,MaClasse.calculer(1,1));
                }
    }
"""
    codeEtu = """public class MaClasse{
        public static int calculer(int a, int b) {
        int res = a + b;
        if (a == 0){
          res = b * 2;
        }

        if (b == 0) {
          res = a * a;
        }
        return res;
     }
    }
"""

    testeur = TesteurJava(codeEns, codeEtu,True)
    print(testeur.test())
