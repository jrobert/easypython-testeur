__author__ = 'julien'

import os
from ..Testeur import Testeur


class TesteurGenerique(Testeur):
    
    nom = "generique"

    def writeTestFiles(self, directory):
        with open(os.path.join(directory, "makefile"), "w") as file:
            file.write("all:\n\tpython3 ModuleEns.py < solution_etudiant\n\n")
            file.write("infos:\n\techo {}")

        with open(os.path.join(directory, "ModuleEns.py"), "w", encoding="utf-8") as file:
            file.write(self.codeTest.decode())
        with open(os.path.join(directory, "solution_etudiant"), "w", encoding="utf-8") as file:
            file.write(self.codeATester)
