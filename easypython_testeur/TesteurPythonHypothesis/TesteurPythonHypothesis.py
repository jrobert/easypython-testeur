__author__ = 'julien + florent'

import os
import shutil
from ..Testeur import Testeur

PRELUDE = """
def solution(fun):
    fun.solution=True
    return fun

attendu_etudiant = None
"""


class TesteurPythonHypothesis(Testeur):
    """
    Un testeur à utiliser avec la bibliothèque Hypothesis (hypothesis.readthedocs.io),
    ou toute autre bibliothèque de tests (sous réserve).
    Les tests pour le code étudiant sont définis dans l'énoncé, leur nom doit être 'test_XXX'
    Quand une propriété n'est pas satisfaite, le test doit appeler la fonction 'fail_test.fail_test'.
    Soit une (ou plusieurs) fonctions solutions sont incluses, avec l'attribut '@solution', soit
    le module énoncé doit contenir une liste de chaînes nommée 'attendu_etudiant' qui est la liste
    des noms des fonctions que l'étudiant·e doit implémenter.
    """
    
    nom = "pythonHypothesis"

    def writeTestFiles(self, directory):
        with open(os.path.join(directory, "makefile"), "w") as file:
            file.write("all:\n\tpython3 scriptTesteurPythonHypothesis.py\n\n")
            file.write("infos:\n\tpython3 scriptTesteurPythonHypothesis.py --infos\n\n")

        with open(os.path.join(directory, "ModuleEns.py"), "w", encoding="utf-8") as file:
            file.write(PRELUDE)
            file.write(self.codeTest.decode())
        with open(os.path.join(directory, "ModuleEtu.py"), "w", encoding="utf-8") as file:
            file.write(self.codeATester)
        for f in ['fail_test.py', 'auto_hypothesis.py', 'scriptTesteurPythonHypothesis.py']:
            shutil.copyfile(os.path.join(os.path.dirname(__file__), f),
                        os.path.join(directory,f))
 
