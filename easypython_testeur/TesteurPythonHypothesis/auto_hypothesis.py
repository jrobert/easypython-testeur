import hypothesis.strategies as st
from string import printable


class ConteneurEntréesVide(Exception):
    pass

def stratégie_dict(stratégie_tuples_clé_val):
    (stratégie_clé, stratégie_valeurs) = stratégie_tuples_clé_val.wrapped_strategy.element_strategies
    return st.dictionaries(stratégie_clé, stratégie_valeurs)

def _trouver_stratégie_conteneur(entrée, stratégie_cont):
    if len(entrée) == 0:
        raise ConteneurEntréesVide # on ne sait pas quels éléments mettre dans le conteneur
    types_éléments = frozenset(type(e) for e in entrée)
    if len(types_éléments) > 1:
        raise ValueError("Pas de stratégie auto pour les conteneurs hétérogènes")
    for e in entrée:
        try:
            stratégie_éléments = _trouver_stratégie_aux(e)
            return stratégie_cont(stratégie_éléments)
        except ConteneurEntréesVide:
            pass


def _trouver_stratégie_aux(entrée):
    if isinstance(entrée, tuple):
        stratégies = (_trouver_stratégie_aux(e) for e in entrée)
        return st.tuples(*stratégies)
    elif isinstance(entrée, int):
        return st.integers()
    elif isinstance(entrée, str):
        return st.text(printable)
    elif isinstance(entrée, list):
        return _trouver_stratégie_conteneur(entrée, st.lists)
    elif isinstance(entrée, set):
        return _trouver_stratégie_conteneur(entrée, st.sets)
    elif isinstance(entrée, dict):
        return _trouver_stratégie_conteneur(entrée.items(), stratégie_dict)
    elif isinstance(entrée, float):
        raise ValueError("Pas de stratégie auto pour des flottants (besoin d'==)")
    else:
        raise ValueError("Pas de stratégies auto pour le type %s" % type(entrée))

def trouver_stratégie(entrées):
    for entrée in entrées:
            try:
                return _trouver_stratégie_aux(entrée)
            except ConteneurEntréesVide:
                pass
    raise ValueError("Trop de sous-structures vides pour déterminer une stratégie")
    
# def test_stratégie_cohérente(entrées):
#     notre_stratégie = trouver_stratégie(entrées)
#     assert True
