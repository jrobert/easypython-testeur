#!/usr/bin/env python
"""
Module contenant la classe Testeur
"""
import abc
import subprocess
import tempfile
import jsonpickle

Testeurs = {} 

def register_testeur(classe_testeur):
    if classe_testeur.nom:
        Testeurs[classe_testeur.nom] = classe_testeur

class RegisterTesteurMetaClass(type):
    def __new__(cls, clsname, bases, attrs):
        newclass = super(RegisterTesteurMetaClass, cls).__new__(cls, clsname, bases, attrs)
        register_testeur(newclass)
        return newclass

def loads(chaine):
    return jsonpickle.loads(chaine, keys=True)


class Testeur(metaclass = RegisterTesteurMetaClass):
    """
    Classe représentant un Testeur :
        une boite noire qui doit savoir générer un ma
        makefile avec une option all et une option infos .
        Chaque option doit exécuter du code qui affiche du json
    """
    nom = None

    def __init__(self, codeTest, codeATester, in_docker=True, **kwargs):
        self.codeTest = codeTest
        self.codeATester = codeATester
        self.metainfos = kwargs
        self.in_docker = in_docker

    @abc.abstractmethod
    def writeTestFiles(self, directory):
        return

    def test(self):
        with tempfile.TemporaryDirectory(prefix="/opt/tmp/") as tmpdirname:
            self.writeTestFiles(tmpdirname)
            if self.in_docker:
                subprocess.run("chown -R 1000:1000 " + tmpdirname, shell=True)
                commande = "'cd " + tmpdirname + "; timeout 15s make -s all'"
                sortie = subprocess.run("docker run -u 1000 --rm -t -v " + tmpdirname + ":" + tmpdirname +
                                        " easypython bash -c " + commande, shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            else:
                sortie = subprocess.run(
                    "cd " + tmpdirname + "; make -s all", shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            if sortie.returncode!=0:
                return {"_valide":False,"_messages":{"erreur interne":["STDOUT:"+sortie.stdout.decode(), "STDERR:"+sortie.stderr.decode()]}}
            try:
               return loads(sortie.stdout.decode())
            except Exception as e:
                return {"_valide":False,"_messages":{"erreur interne":["STDOUT:"+sortie.stdout.decode(), "STDERR:"+sortie.stderr.decode(), "codeTest"+str(self.codeTest), "codeATester"+str(self.codeATester)]}}

    def infos(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            self.writeTestFiles(tmpdirname)
            sortie = subprocess.run(
                "cd " + tmpdirname + "; make -s infos", shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            if sortie.returncode!=0:
                return {"erreurs":sortie.stderr.decode()}
            return loads(sortie.stdout.decode())
