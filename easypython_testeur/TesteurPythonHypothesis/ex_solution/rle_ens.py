from hypothesis import given
from hypothesis.strategies import text
from fail_test import fail_test


@solution
def encode(input_string):
    if not input_string:
        return []
    count = 1
    prev = ''
    lst = []
    for character in input_string:
        if character != prev:
            if prev:
                entry = (prev, count)
                lst.append(entry)
            count = 1
            prev = character
        else:
            count += 1
    else:
        entry = (character, count)
        lst.append(entry)
    return lst

@solution
def decode(lst):
    q = ''
    for character, count in lst:
        q += character * count
    return q


@given(s=text())
def test_decode_encode_inverse(code_etu, s):
    round_trip = code_etu.decode(code_etu.encode(s))
    if s != round_trip:
        fail_test("encoder \"{}\" puis la décoder ne redonne pas la même valeur".format(s))


