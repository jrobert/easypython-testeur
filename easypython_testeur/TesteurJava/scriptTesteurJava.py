__author__ = 'julien'


import os
import subprocess
import json
import resource,psutil
import sys

nomClasse = sys.argv[1]
nomClasseTest = sys.argv[2]


def limite_resources():
        pid = os.getpid()
        ps = psutil.Process(pid)
        resource.setrlimit(resource.RLIMIT_CPU, (4, 4))


class ErreurCompilation(Exception):
    def __init__(self, value):
        self.value = value
    def getInfos(self):
        return self.value

def compiler(nomClasse, classpath):
        process = subprocess.Popen(["java","-cp",classpath, "easypython.CompileJava", nomClasse+".java"], stdout=subprocess.PIPE,stderr=subprocess.PIPE, preexec_fn=limite_resources)
        output, erreur_compilation_solution = process.communicate()
        output = output.decode()
        if output and json.loads(output) :
           res = json.loads(output)
           res["_messages"]["Erreur de compilation dans le fichier " + nomClasse+".java"] = res["_messages"].get("Erreur de compilation","toto")
           del res["_messages"]["Erreur de compilation"]
           raise ErreurCompilation(res)
        if erreur_compilation_solution or process.returncode<0:
            resultat ={"Erreur de compilation dans le fichier " + nomClasse+".java" :[erreur_compilation_solution.decode() if erreur_compilation_solution else "Arret de la compilation qui prenait trop de temps."] }
            raise ErreurCompilation({"_valide":False, "_messages":resultat})
        
def tester():
        resultat = {}
        classpath=".:"+"hamcrest-core-1.3.jar:"+"junit-4.12.jar:"+"easypython.jar:"+"json-20171018.jar"
        try:
            compiler(nomClasse, classpath)
            compiler(nomClasseTest, classpath)
            compiler("TesteurEasyPython", classpath)
        except ErreurCompilation as e:
            return e.getInfos()
        #except Exception as e:
        #    return {"_valide":False, "_messages":["erreur : " + str(e)]}
        #process = subprocess.Popen(["javac","-encoding","utf-8","-cp",classpath,nomClasse+".java"], stdout=subprocess.PIPE,stderr=subprocess.PIPE, preexec_fn=limite_resources)


        #process = subprocess.Popen(["javac","-encoding","utf-8","-cp",classpath,nomClasseTest+".java"], stdout=subprocess.PIPE,stderr=subprocess.PIPE, preexec_fn=limite_resources)
        #output, erreur_compilation_test = process.communicate()
        #if erreur_compilation_test or process.returncode<0:
        #    resultat["Erreur de compilation"]=[erreur_compilation_test.decode() if erreur_compilation_test else "Arret de la compilation qui prenait trop de temps." ]
        #   return {"_valide":False, "_messages":resultat}

        #process = subprocess.Popen(["javac","-encoding","utf-8","-cp",classpath,"TesteurEasyPython.java"], stdout=subprocess.PIPE,stderr=subprocess.PIPE, preexec_fn=limite_resources)
        #output, erreur_compilation_test = process.communicate()
        #if erreur_compilation_test or process.returncode<0:
        #    resultat["Erreur de compilation"]=[erreur_compilation_test.decode() if erreur_compilation_test else "Arret de la compilation qui prenait trop de temps." ]
        #   return {"_valide":False, "_messages":resultat}

        process = subprocess.Popen(["java","-cp",classpath,"TesteurEasyPython"], stdout=subprocess.PIPE,stderr=subprocess.PIPE, preexec_fn=limite_resources)
        output, erreur_execution = process.communicate()

        if output:
            resultat["Votre code ne fait pas ce qui est attendu"]=[]
            for line in output.decode().splitlines():
                resultat["Votre code ne fait pas ce qui est attendu"].append(line)
            return {"_valide":False, "_messages":resultat}
        if erreur_execution:
            resultat["Erreur à l'exécution"]=[erreur_execution.decode()] # Est ce que ça peut arriver ??
            return {"_valide":False, "_messages":resultat}
        if process.returncode<0:
            resultat["Erreur à l'exécution"]=["Votre code a du être arrêté car prenait trop de temps"]
            return {"_valide":False, "_messages":resultat}
        return {"_valide":True, "_messages":{}}


if __name__ == "__main__":
    print(json.dumps(tester()))
