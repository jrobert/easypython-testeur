class TestFailure(Exception):
    def __init__(self, *msg):
        self.msg = msg

def fail_test(*msg):
    raise TestFailure(*msg)
