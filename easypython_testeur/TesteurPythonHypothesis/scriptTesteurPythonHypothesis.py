__author__ = 'julien + Florent'

import importlib
import sys
import traceback
import inspect
import time
import jsonpickle
from fail_test import TestFailure
from hypothesis import given
import auto_hypothesis



class resultats:
    def __init__(self):
        self._valide = True
        self._messages = {}
        self._temps = None

    def invalide(self, raison, message):
        self._valide = False
        if raison in self._messages:
            self._messages[raison].append(message)
        else:
            self._messages[raison] = [message]

    def temps(self, t):
        self.temps = t

    def __repr__(self):
        if self._valide:
            return str(self._messages)
        return str(self._messages)

    def dumps(self):
        return jsonpickle.dumps(self.__dict__, keys=True)

    def to_dict(self):
        return self.__dict__

    def loads(chaine):
        d = jsonpickle.loads(chaine, keys=True)
        self._valide = d["_valide"]
        self._messages = d["_messages"]
        self._temps = d["_temps"]


old_stdout = None


def drop_stdout(f):
    def g(*args, **kwargs):
        global old_stdout
        try:
            (old_stdout, sys.stdout) = (sys.stdout, old_stdout)
            return f(*args, **kwargs)
        finally:
            (old_stdout, sys.stdout) = (sys.stdout, old_stdout)
    return g


class Erreur(Exception):
    def __init__(self, erreur, exception):
        self.erreur = erreur
        self.exception = exception


def remplir_ex(e, pile, limit=None):
    exc_type, exc_value, exc_traceback = sys.exc_info()
    i = 0
    while(exc_traceback.tb_next and i < pile):
        i = i + 1
        exc_traceback = exc_traceback.tb_next
    fe = traceback.format_exception(
        exc_type, exc_value, exc_traceback, limit=limit)
    return (Erreur(str(e), str(''.join(fe))))

# reste pour l'exemple ? (à supprimer ?)
# def remplir_ex_syn(e):
#    return remplir_ex(e,100,limit=0)


# Prend en entree un nom de fichier et renvoie le module ouvert du fichier
@drop_stdout
def ouvre_module(fichier):
    try:
        module = importlib.import_module(fichier)
        return module
    except Exception as e:
        ex = remplir_ex(e, 100, limit=0)
    raise ex


@drop_stdout
def run_test(codeEtu, test):
    """
    :param codeEtu: le dictionnaire des fonctions fournies par l'étudiant·e
    :param tests: un test à passer
    :return: ????
    Lève une exception si besoin.
    """
    pile = traceback.extract_stack()
    try:
        res = []
        test(codeEtu)
        return res
    except TestFailure as m:
        raise m
    except Exception as e:
        ex = remplir_ex(e, len(pile) + 6)
        raise ex

@drop_stdout
def run_jeu_entrees(fonction, entrees):
    """
    :param fonction:
    :param entrees: une liste d'entrees
    :return: liste de couples (entree,sortie)
    Leve une exception si besoin.
    """
    pile = traceback.extract_stack()
    try:
        res = []
        for i in entrees:
            res.append((i, fonction(*i)))
            return res
    except Exception as e:
        ex = remplir_ex(e, len(pile) + 3)
        raise ex

class CodeEtu:
    # Cet objet est rempli dynamiquement avec les fonctions fournies dans le fichier étudiant.
    pass
    
class ExercicePythonHypothesis:
    def charger_module(self, nom_module):
        try:
            self.module_ens = ouvre_module(nom_module)
            return True
        except Erreur as e:
            self.messagesErreur.append(str(e))
            return False

    def creer_tests_old_school(self, solution):
        entrees_visibles = self.module_ens.__dict__["entrees_visibles"]
        entrees_invisibles = self.module_ens.__dict__.get("entrees_invisibles", [])
        fonction_ens = getattr(self.module_ens, solution)

        try:
            solutions_visibles = run_jeu_entrees(fonction_ens, entrees_visibles)
            solutions_invisibles = run_jeu_entrees(fonction_ens, entrees_invisibles)
        except Erreur as e:
            self.error = True
            self.messagesErreur.append(str(e))
            return []

        def test_invisible(code_etu):
            resultat = resultats()
            time0 = time.time()
            fonction_etudiant = getattr(code_etu, solution)
            soletu_invi = run_jeu_entrees(fonction_etudiant, entrees_invisibles)
            temps = time.time() - time0
            resultat.temps(temps)
            for ((ent_ens, sor_ens), (_, sor_etu)) in zip(solutions_invisibles, soletu_invi):
                 if sor_ens != sor_etu:
                     raise TestFailure("Votre fonction ne fait pas ce qui est attendu",
                                       "Votre fonction ne renvoie pas ce qui est attendu pour au moins une entrée invisible")
            return resultat

        def test_visible(code_etu):
            resultat = resultats()
            time0 = time.time()
            fonction_etudiant = getattr(code_etu, solution)
            soletu_vi = run_jeu_entrees(fonction_etudiant, entrees_visibles)
            temps = time.time() - time0
            resultat.temps(temps)
            for ((ent_ens, sor_ens), (_, sor_etu)) in zip(solutions_visibles, soletu_vi):
                if sor_ens != sor_etu:
                    raise TestFailure("Votre fonction ne fait pas ce qui est attendu", "Sur l'entrée " + repr(
                        ent_ens) + " votre programme a renvoyé " + repr(sor_etu) + " alors qu'on attendait " + repr(sor_ens))
            return resultat

        try:
            stratégie_auto = auto_hypothesis.trouver_stratégie(entrees_visibles + entrees_invisibles)
        except ValueError:
            return [test_visible, test_invisible]

        @given(entrée_auto=stratégie_auto)
        def test_stratégie_auto(code_etu, entrée_auto):
            fonction_etudiant = getattr(code_etu, solution)
            sortie_étu = fonction_etudiant(*entrée_auto)
            sortie_ens = fonction_ens(*entrée_auto)
            if sortie_étu != sortie_ens:
                raise TestFailure("Votre fonction ne fait pas ce qui est attendu",
                                  "Votre fonction ne renvoie pas ce qui est attendu pour au moins une entrée magique")
        return [test_visible, test_invisible, test_stratégie_auto]

    def parser_module(self):
        self.fonctions_attendues = self.module_ens.__dict__.get('attendu_etudiant', [])
        
        solutions = \
          { nom: fun for nom, fun in self.module_ens.__dict__.items()\
            if "solution" in dir(fun)}

        tests = \
            [ fun for nom, fun in self.module_ens.__dict__.items()\
              if nom.startswith("test_")]

        has_old_school = len(solutions) == 1 and ("entrees_visibles" in self.module_ens.__dict__ or "entrees_invisibles" in self.module_ens.__dict__)
        if has_old_school:
            tests_old_school = self.creer_tests_old_school(next(iter(solutions)))
            tests.extend(tests_old_school)

        if self.fonctions_attendues:
            if solutions:
                if (self.fonctions_attendues == list(solutions.keys())):
                    self.solutions_ens = solutions
                else:
                    self.messagesErreur.append("ERREUR: vos solutions (%s) ne sont pas cohérente avec le travail étudiant attendu (%s)" % (solutions.keys, self.fonctions_attendues))
                    return False
        else:
            if solutions:
                self.fonctions_attendues = solutions.keys()
            else:
                self.messagesErreur.append("ERREUR : vous ne demandez pas de travail à l'étudiant·e !")
                self.messagesErreur.append("NOTE: le decorateur solution ne doit pas etre défini")
                return False
                
        if tests:
            self.tests = tests
        else:
            self.messagesErreur.append("ERREUR: vous n\'avez pas défini de tests")
            return False

        self.messages.append("Solution ou nom des fonctions étudiant + tests, tout y est !")
        return True

    def contient_solution_ens(self):
        return bool(self.solutions)
    
    def tester_solution_ens(self):
        try:
            t = CodeEtu()
            for fname in self.fonctions_attendues:
                t.__dict__[fname] = getattr(self.module_ens, fname)
                    
            time0 = time.time()
            for test in self.tests:
                test(t)
            temps = time.time() - time0
            self.temps = temps
            return True
        except TestFailure as m:
            self.error = True
            self.messageErreur.append(e.msg)
            raise m
        except Erreur as e:
            self.error = True
            self.messagesErreur.append(str(e))
            return False

    def afficher(self):
       print(self.toDict())

    def toDict(self):
        attrs = ["messages", "messagesErreur", "messagesInfo", "temps"]
        res = {attr: self.__dict__[attr]
               for attr in attrs if self.__dict__[attr]}
        return res

    def __init__(self, module, debug=False, log=None):
        self.debug = debug and log
        self.log = log
        self.messages = []
        self.messagesErreur = []
        self.messagesInfo = []
        self.module_ens = None
        self.temps = None
        self.module_charge = False
        self.solutions = {}
        self.fonctions_attendues = []
        if (self.charger_module(module) and self.parser_module()):
            self.module_charge = True
        if self.solutions:
            self.tester_solution_ens()
        if len(self.fonctions_attendues) == 0:
            self.error = True
            self.messagesErreur.append("fonctions_attendues: %s" % self.fonctions_attendues)

    def tester_solution_etu(self, nom_module_etu):
        resultat = resultats()
        if not self.module_charge:
            resultat.invalide("Erreur", "Impossible de charger la solution")
            return resultat
        try:
            module_etu = ouvre_module(nom_module_etu)
        except Erreur as e:
            resultat.invalide("Erreur", e.exception + " " + e.erreur)
            return resultat

        leCodeEtu = CodeEtu()
        for fname in self.fonctions_attendues:
            if fname not in dir(module_etu):
                resultat.invalide("Vous n'avez pas respecté l'énoncé",
                                  "Votre programme doit contenir une fonction " + fname)
                return resultat
            else:
                leCodeEtu.__dict__[fname] = getattr(module_etu, fname)

        try:
            time0 = time.time()
            for test in self.tests:
                run_test(leCodeEtu, test)
            temps = time.time() - time0
            resultat.temps(temps)
        except TestFailure as f:
            resultat.invalide("Propriétés non satisfaites:", f.msg)
            return resultat
        except Erreur as e:
            resultat.invalide("Erreur d'exécution",
                              e.exception + " " + e.erreur)
            return resultat
        return resultat


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--infos", help="renvoie des informations sur le module enseignant", action="store_true")
    parser.add_argument(
        "--module-ens", help="nom du module enseignant", dest="ModuleEns")
    parser.add_argument(
        "--module-etu", help="nom du module étudiant", dest="ModuleEtu")
    args = parser.parse_args()
    ModuleEns = args.ModuleEns or "ModuleEns"
    ModuleEtu = args.ModuleEtu or "ModuleEtu"
    if args.infos:
        e = ExercicePythonHypothesis(ModuleEns)
        if e.contient_solution_ens():
            e.tester_solution_ens()
        print(jsonpickle.dumps(e.toDict(), keys=True))
    else:
        e = ExercicePythonHypothesis(ModuleEns)
        if e.contient_solution_ens():
            e.tester_solution_ens()
        print(e.tester_solution_etu(ModuleEtu).dumps())
