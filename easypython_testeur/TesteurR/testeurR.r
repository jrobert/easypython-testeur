#!/usr/bin/Rscript --vanilla --slave

argv <- commandArgs(TRUE)
#récupération du fichier de réponse
fic_ens <- argv[1]
fic_etu <- argv[2]

to_string_exerciseur <- function(valide, titre, message){
    if(valide)
			return('{"_valide":true}')
		return(paste('{"_valide":false, "_messages":{"',titre,'":["',message,'"]}}',sep=""));
}


feedback<-function(l,res){
    l2<-l
    l2["reponse"]<-NULL
    arguments<-paste(sapply(l2,paste,collapse=" "),collapse=",")
    #vérification du type renvoyé
    if(class(l$reponse)!=class(res))
    {
        erreurs <- paste("l'appel de la fonction sur ",arguments," renvoie un objet de type ",class(res)," alors que la réponse attendue est de type ",class(l$reponse),sep="")
        return(to_string_exerciseur(FALSE,"Erreur de typage",erreurs))
    }
    
    #vérification de la taille de l'objet renvoyé
    if(length(l$reponse)!=length(res))
    {
        erreurs <- (paste("l'appel de la fonction sur ",arguments," renvoie un objet de longueur ",length(res)," alors que la réponse attendue est de longueur ",length(l$reponse),sep=""))
        return(to_string_exerciseur(FALSE,"Erreur de longueur",erreurs))
    }
    
    #vérification de la valeur l'objet renvoyé
    if(!all(l$reponse==res))
    {
        erreurs <- paste("l'appel de la fonction sur ",arguments," renvoie ",paste(res,collapse=" ")," alors que la réponse attendue est ",paste(l$reponse,collapse=" "),sep="")
        return(to_string_exerciseur(FALSE,"Erreur de fonctionnalité",erreurs))
    }
    
    else{
         return(NULL)
    }
    
}

exo11<-function(solEns,solEtu){
#fonction attendue :
        env_ens <- new.env()
        env_etu <- new.env()
	source(solEns,local=env_ens)
	source(solEtu,local=env_etu)

#on commence par tester l'existance de la fonction
        if( !exists("nom_solution",env_ens))
		return( to_string_exerciseur(FALSE,"Erreur dans l'énoncé","La fonction demandée n'est pas précisée") ) 
        if( !exists(env_ens$nom_solution,env_ens))
		return( to_string_exerciseur(FALSE,"Erreur dans l'énoncé","La fonction demandée n'est pas fournie par l'enseignant") ) 


        if( !exists(env_ens$nom_solution,env_etu)){
		return( to_string_exerciseur(FALSE,"Erreur de spécification","Vous n'avez pas défini la fonction demandée") ) 
	}
        sol_ens <- eval(parse(text=paste("env_ens$",env_ens$nom_solution,sep="")))
        sol_etu <- eval(parse(text=paste("env_etu$",env_ens$nom_solution,sep="")))
        
	if( length(formals(sol_etu)) != length(formals(sol_ens)) ){
		return( to_string_exerciseur(FALSE,"Erreur de spécification","Le nombre d'arguments de votre fonction n'est pas bon.") ) 
	}

#on vérifie que la fonction proposée utilise la fonction table()
	if(length(grep("table\\(",readLines(solEtu)))==0) 
		return( to_string_exerciseur(FALSE,"Erreur de spécification","Votre fonction doit utiliser la fonction table()") ) 

#on construit un jeu de tests visibles
	testsv<-list(
			list(arg1=2,arg2=c(1,2,1,3,4,1,2),reponse=sol_etu(2,c(1,2,1,3,4,1,2))),
			list(arg1='a',arg2=c('a','b','a','d','a'),reponse=sol_etu('a',c('a','b','a','d','a')))
			)


#on vérifie la fonction sur le jeu de tests
	res<-try(unlist(lapply(testsv,function(l)return(feedback(l,sol_ens(l$arg1,l$arg2)))))[1])
	if(class(res)=="try-error")
		return(to_string_exerciseur(FALSE,"Erreur",res[1]))

	res<-unlist(lapply(testsv,function(l)return(feedback(l,sol_ens(l$arg1,l$arg2)))))[1]
	if(is.null(res))
		return(to_string_exerciseur(TRUE,"",""))
	else
		return(res)
}



cat(exo11(fic_ens,fic_etu), "\n",sep="")

